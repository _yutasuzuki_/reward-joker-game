import { useEffect, useState } from 'react'
import { database } from 'lib/firebase'

const baseState: ResultState = {
  dones: [],
  discards: {},
  rewards: {}
}

export const useResult = (room: string, user: UserState) => {
  const [resultLoaded, setResultLoaded] = useState<boolean>(false)
  const [result, setResult] = useState<ResultState>()

  useEffect(() => {
    const handler = (snapshot) => {
      const data = snapshot.val() as ResultState
      if (data) {
        setResult(Object.assign({}, baseState, data))
        setResultLoaded(true)
      }
    }
    const mount = async () => {
      if (room && user) {
        database.ref(`results/${room}`).on('value', handler)
        database.ref(`results/${room}`).on('child_removed', handler)
        const snapshot = await database.ref(`results/${room}`).get()
        const data = snapshot.val() as ResultState
        setResult(Object.assign({}, baseState, data))
        setResultLoaded(true)
      }
    }
    mount()

    return () => {
      database.ref(`results/${room}`).off('value', handler)
      database.ref(`results/${room}`).on('child_removed', handler)
    }
  }, [room, user])

  return { result, resultLoaded }
}