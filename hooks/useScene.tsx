import { useEffect, useState } from 'react'
import { database } from 'lib/firebase'

export const useScene = (room: string, user: UserState) => {
  const [sceneLoaded, setSceneLoaded] = useState<boolean>(false)
  const [scene, setScene] = useState<SceneState>(null)

  useEffect(() => {
    const handler = (snapshot) => {
      const data = snapshot.val() as SceneState
      if (room && user.userId) {
        if (data) {
          setScene(data)
        }
        setSceneLoaded(true)
      }
    }
    const mount = async () => {
      if (room && user.userId) {
        database.ref(`scenes/${room}`).on('value', handler)
        const res = await database.ref(`scenes/${room}`).get()
        const data = res.val() as SceneState
        setScene(data)
      }
    }
    mount()
    return () => {
      database.ref(`scenes/${room}`).off('value', handler)
    }
  }, [room, user])


  return { scene, sceneLoaded }
}