import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { useRouter } from 'next/router'

export const useAuth = () => {
  const [room, setRoom] = useState<string>(null)
  const [user, setUser] = useState<UserState>({ userId: null, pictureUrl: null, displayName: null })
  const router = useRouter()

  useEffect(() => {
    const mount = async () => {
      const liff = (await import('@line/liff')).default
      await liff.init({ liffId: process.env.liffId })
      const context = liff.getContext()
      if (!context) {
        router.push('/lp')
        return 
      }
      let r = ''
      if (context.type === "group") {
        r = `group_${context.groupId}`
      } else if (context.type === "room") {
        r = `room_${context.roomId}`
      } else {
        r = 'debugging_43mkdfs093csdsdfnil'
        if (process.env.NODE_ENV !== 'development') {
          router.push('/lp')
        }
      }
      if (!liff.isLoggedIn()) {
        liff.login({})
      }
      const u = await liff.getProfile() as UserState
      setUser(u)
      setRoom(r)
    }
    mount()
    return () => {}
  }, [])

  return { room, user }
}