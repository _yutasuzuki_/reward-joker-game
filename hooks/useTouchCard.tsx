import { useCallback, useEffect, useState } from 'react'
import { database } from 'lib/firebase'

export const useTouchCard = (room: string, user: UserState) => {
  const [touchCard, setTouchCard] = useState<TouchCardState>(null)

  useEffect(() => {
    const handler = (snapshot) => {
      const data = snapshot.val() as TouchCardState
      if (room && user.userId) {
        setTouchCard(data)
      }
    }
    const mount = async () => {
      if (room && user.userId) {
        database.ref(`touchCard/${room}`).on('value', handler)
        const res = await database.ref(`touchCard/${room}`).get()
        const data = res.val() as TouchCardState
        setTouchCard(data)
      }
    }
    mount()
    return () => {
      database.ref(`touchCard/${room}`).off('value', handler)
    }
  }, [room, user])

  return { touchCard }
}