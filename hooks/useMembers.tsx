import { useEffect, useState } from 'react'
import { database } from 'lib/firebase'

export const useMembers = (room: string, user: UserState) => {
  const [membersLoaded, setMembersLoaded] = useState<boolean>(false)
  const [members, setArrayMembers] = useState<UserState[]>([])

  useEffect(() => {
    const handler = (snapshot) => {
      const data = snapshot.val() as UserState[] || []
      if (data) {
        setArrayMembers(data)
        setMembersLoaded(true)
      } 
    }
    const mount = async () => {
      if (room && user) {
        database.ref(`members/${room}`).on('value', handler)          
      }
    }
    mount()

    return () => {
      database.ref(`members/${room}`).off('value', handler) 
    }
  }, [room, user])

  return { members, membersLoaded }
}