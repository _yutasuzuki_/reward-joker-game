import { useEffect, useState } from 'react'
import { database } from 'lib/firebase'

const baseState: GameState = {
  current_user: null,
  order: [],
  cards: {}
}


export const useGame = (room: string, user: UserState) => {
  const [gameLoaded, setGameLoaded] = useState<boolean>(false)
  const [game, setGame] = useState<GameState>(baseState)

  useEffect(() => {
    const handler = (snapshot) => {
      const data = snapshot.val() as GameState
      if (data) {
        setGame({ ...baseState, ...data })
        setGameLoaded(true)
      }
    }
    const mount = async () => {
      if (room && user) {
        database.ref(`games/${room}`).on('value', handler)
        const snapshot = await database.ref(`games/${room}`).get()
        const data = snapshot.val() as GameState
        setGame({ ...baseState, ...data })
        setGameLoaded(true)       
      }
    }
    mount()

    return () => {
      database.ref(`games/${room}`).off('value', handler) 
    }
  }, [room, user])

  return { game, gameLoaded }
}