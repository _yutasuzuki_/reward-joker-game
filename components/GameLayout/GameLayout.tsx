import Head from 'next/head'
import styles from './GameLayout.module.css'
import Link from 'next/link'
import { useCallback, useEffect, useState } from 'react'
import { database } from 'lib/firebase' 
import { useRouter } from 'next/router'
import { Drawer } from 'components/Drawer/Drawer'

type Props = {
  room: string
}

export const GameLayout: React.FC<Props> = ({ children, room }) => {
  const [drawer, setDrawer] = useState<boolean>(false)

  const _handleOnExit = useCallback(() => {
    if (window.confirm("ゲームを終了しますか？")) {
      database.ref(`games/${room}`).remove()
      database.ref(`members/${room}`).remove()
      database.ref(`cards/${room}`).remove()
      database.ref(`results/${room}`).remove()
      database.ref(`scenes/${room}`).set({ scene: 0, members: [] })
      setDrawer(false)
    }
  }, [room])

  const _handleOnClose = useCallback(() => {
    setDrawer(false)
  }, [])

  return (
    <>
      <Head>
        <title>Reward Joker Game - 報酬ババ抜き</title>
        <meta name="description" content="Reward Joker Game - 報酬ババ抜き" />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:image" content="https://0ops.dev/images/ogp.jpg" />
        <meta name="og:title" content="Reward Joker Game" />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <main className={styles.main}>{children}</main>
      <a className={styles.btnMenu} onClick={() => setDrawer(true)}><span /></a>
      <Drawer show={drawer} onClose={_handleOnClose}>
        <div className={styles.drawerContainer}>
          <a onClick={_handleOnExit} className={styles.exit}>強制終了</a>
        </div>
      </Drawer>
    </>
  )
}
