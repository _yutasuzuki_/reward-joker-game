import Head from 'next/head'
import styles from './Drawer.module.css'
import Link from 'next/link'
import { useCallback, useEffect } from 'react'
import { database } from 'lib/firebase' 
import { useRouter } from 'next/router'

type Props = {
  show: boolean
  onClose(): void
}

export const Drawer: React.FC<Props> = ({ children, show, onClose }) => {

  return (
    <>
      <div className={`${styles.drawer} ${show ? styles.isShowDrawer : ''}`}>
        {children}
      </div>
      <a className={`${styles.overlay} ${show ? styles.isShowOverlay : ''}`} onClick={onClose} />
    </>
  )
}
