import styles from './Modal.module.css'
import utilStyles from 'styles/utils.module.css'
import { useCallback } from 'react'

type Props = {
  show: boolean
  onClose?(): void
}

export const Modal: React.FC<Props> = ({ show, children, onClose }) => {
  const _handleOnClose = useCallback(() => {
    if (onClose) {
      onClose()
    }
  }, [onClose])
  return show && (
    <div className={styles.overlay} onClick={_handleOnClose} style={onClose && { pointerEvents: 'auto' }}>
      <div className={styles.container}>{children}</div>
    </div>
  )
}
