import styles from './FacedownCard.module.css'
import utilStyles from 'styles/utils.module.css'

type Props = {
  card: CardState
}

export const FacedownCard: React.FC<Props> = ({ card }) => {
  return (
    <div className={styles.container}>
      <div className={styles.inner} />
    </div>
  )
}
