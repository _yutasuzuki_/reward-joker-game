import styles from './UserRank.module.css'
import utilStyles from 'styles/utils.module.css'
import { useMemo } from 'react'

type Props = {
  index: number
  user: UserState
  reward: CardState[]
}

export const UserRank: React.FC<Props> = ({ user, index, reward }) => {

  const rewards = useMemo(() => {
    return reward.filter(card => card.text)
      .map((card, index) => {
        return (
          <li key={index} className={styles.rewardItem}>
            <div className={styles.rewardText}>{card.text}</div>
            <div className={styles.rewardAuther}>{card.user.displayName}</div>
          </li>
        )
      })
  }, [reward])

  return (
    <li className={styles.container}>
      <div className={styles.user}>
        <div className={styles.rank}>{index + 1}位</div>
        <div>
          <div className={styles.thumbnail}>
            <img src={user.pictureUrl} />
          </div>
        </div>
        <div className={styles.name}>{user.displayName}</div>
      </div>
      <div className={styles.rewards}>
        <div className={styles.rewardsTitle}>獲得報酬</div>
          {rewards.length ? (
            <ul className={styles.rewardList}>
              {rewards}
            </ul>
          ) : (
            <div className={styles.text}>獲得した報酬はありません</div>
          )}
      </div>
    </li>
  )
}
