import styles from './FaceupCard.module.css'

type Props = {
  card: CardState
  onClick?(card: CardState): void
}

export const FaceupCard: React.FC<Props> = ({ card, onClick }) => {
  return (
    <div className={styles.container} style={{ borderColor: card.colorCode }}>
      {card.id !== 'joker' && (
        <>
          <div className={styles.number} style={{ color: card.colorCode }}>{card.number}</div>
          <div className={styles.text}>{card.text}</div>
        </>
      )}
      {card.id === 'joker' && <div className={styles.joker}><img src="/images/jester.svg" /></div>}
    </div>
  )
}
