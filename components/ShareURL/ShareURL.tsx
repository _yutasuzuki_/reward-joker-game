import styles from './ShareURL.module.css'
import utilStyles from 'styles/utils.module.css'
import { CopyToClipboard } from 'react-copy-to-clipboard'

type Props = {
  onShare(): void
  onCopy(): void
}

export const ShareURL: React.FC<Props> = ({ onShare, onCopy }) => {
  return (
    <section className={styles.cta}>
      <a className={`${utilStyles.btn} ${styles.btnShare}`} onClick={onShare}>
        <img src="/images/line.svg" className={styles.lineLogo} />LINEにURLをシェアする
      </a>
      <CopyToClipboard text={'https://liff.line.me/1655594777-A0eDyaND'} onCopy={onCopy}>
        <div className={styles.copy}>
          <input className={styles.copyInput} defaultValue="https://liff.line.me/1655594777-A0eDyaND" readOnly />
          <a className={styles.copySubmit}>コピー</a>
        </div>
      </CopyToClipboard>
    </section>
  )
}
