import styles from './UserJoin.module.css'
import utilStyles from 'styles/utils.module.css'
import { useMemo } from 'react'

type Props = {
  member: UserState
  user: UserState
  onLeave(): void
}

export const UserJoin: React.FC<Props> = ({ user, member, onLeave }) => {
  return (
    <li className={styles.profile}>
      <div className={styles.thumbnail}>
        <img src={member.pictureUrl} />
      </div>
      <div className={styles.body}>
        <p className={styles.name}>{member.displayName}</p>
        <p className={styles.userId}>{member.userId}</p>
        {member.userId === user.userId && <a className={styles.btnLeave} onClick={onLeave}>参加をやめる</a>}
      </div>
    </li>
  )
}
