import styles from './Discards.module.css'
import { FaceupCard } from 'components/FaceupCard/FaceupCard'
import utilStyles from 'styles/utils.module.css'
import { useMemo } from 'react'

type Props = {
  discards: {
    [key: string]: CardState[]
  }
  rewards: {
    [key: string]: CardState[]
  }
}

export const Discards: React.FC<Props> = (props) => {
  
  const faceupCards = useMemo(() => {
    let discards: CardState[] = []
    let rewards: CardState[] = []
    Object.keys(props.discards).map((key) => {
      discards = [...discards, ...props.discards[key]]
    })
    Object.keys(props.rewards).map((key) => {
      rewards = [...rewards, ...props.rewards[key]]
    })
    const cards = [...discards, ...rewards]
    return cards.map((card, index) => {
      return (
        <li key={index} style={{ top: card.discardStyle.top, left: card.discardStyle.left, zIndex: index }}>
          <div className={styles.item}>
            <div className={styles.card} style={{ transform: `rotate(${card.discardStyle.backRotate})` }}>
              <FaceupCard card={card} />
            </div>
            <div className={styles.card} style={{ transform: `rotate(${card.discardStyle.frontRotate})` }}>
              <FaceupCard card={card} />
            </div>
          </div>
        </li>
      )
    })
  }, [props.discards, props.rewards])

  return (
    <ul className={styles.list}>
      {faceupCards}
    </ul>
  )
}
