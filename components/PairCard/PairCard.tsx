import styles from './PairCard.module.css'
import { FaceupCard } from 'components/FaceupCard/FaceupCard'

type Props = {
  card: CardState
}

export const PairCard: React.FC<Props> = ({ card }) => {
  return (
    <div className={styles.container}>
      <div className={styles.front}>
        <FaceupCard card={card} />
      </div>
      <div className={styles.back}>
        <FaceupCard card={card} />
      </div>
    </div>
  )
}
