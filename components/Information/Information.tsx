import styles from './Information.module.css'
import utilStyles from 'styles/utils.module.css'

type Props = {}

export const FaceupCard: React.FC<Props> = ({ children }) => {
  return (
    <div className={styles.container}>{children}</div>
  )
}
