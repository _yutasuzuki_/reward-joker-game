import Head from 'next/head'
import styles from './GlobalLayout.module.css'
import Link from 'next/link'

type Props = {
}

export const GlobalLayout: React.FC<Props> = ({ children }) => {

  return (
    <>
      <Head>
        <title>Reward Joker Game - 報酬ババ抜き</title>
        <meta name="description" content="Reward Joker Game - 報酬ババ抜き" />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:image" content="https://0ops.dev/images/ogp.jpg" />
        <meta name="og:title" content="Reward Joker Game" />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header className={styles.header}>
        <Link href="/">
          <a className={styles.logo}>Reward Joker Game</a>
        </Link>
      </header>
      <main className={styles.main}>{children}</main>
      <div className={styles.licence}>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
      <footer className={styles.footer}>
        <small className={styles.copyright}>© yutasuzuki</small>
      </footer>
    </>
  )
}
