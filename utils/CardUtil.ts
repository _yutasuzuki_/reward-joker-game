type RemoveDuplicateCards = (cards: CardState[]) => CardState[]
export const removeDuplicateCards: RemoveDuplicateCards = (cards: CardState[]) => {
  return cards
    .map(card => card.id)
    .filter((val, index, self) => self.indexOf(val) === index && index === self.lastIndexOf(val))
    .map((id) => cards.find(card => card.id === id))
}

type GetDuplicateCards = (cards: CardState[]) =>  CardState[]
export const getDuplicateCards: GetDuplicateCards = (cards: CardState[]) => {
  return cards
    .map(card => card.id)
    .filter((val, index, self) => self.indexOf(val) === index && index !== self.lastIndexOf(val))
    .map((id) => cards.find(card => card.id === id))
}

const positionMax = 70
const positionMin = 10
const rotateMin = -45
const rotateMax = 45
type GetDiscardPosition = () => CardState["discardStyle"]
export const getDiscardPosition: GetDiscardPosition = () => {
  const top = Math.floor(Math.random() * (positionMax + 1 - positionMin)) + positionMin + '%'
  const left = Math.floor(Math.random() * (positionMax + 1 - positionMin)) + positionMin + '%'
  const frontRotate = Math.floor(Math.random() * (rotateMin + 1 - rotateMax)) + rotateMax + 'deg'
  const backRotate = Math.floor(Math.random() * (rotateMin + 1 - rotateMax)) + rotateMax + 'deg'
  return { top, left, frontRotate, backRotate }
}