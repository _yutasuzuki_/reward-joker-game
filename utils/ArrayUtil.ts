type RemoveDuplicate = <T>(array: T[], key: string) => T[]
export const removeDuplicate: RemoveDuplicate = (array, key) => {
  return array.filter((element, index, self) => {
    return (self.findIndex(e => e[key] === element[key])) === index
  });
}

// Fisher–Yates shuffle
type Shuffle = <T>(array: T[]) => T[]
export const shuffle: Shuffle = ([...array]) => {
  for (let i = array.length - 1; i >= 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]]
  }
  return array;
}