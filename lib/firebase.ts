import firebase from 'firebase'
import 'firebase/app'
import 'firebase/firestore'

// const firebaseConfig = process.env.NODE_ENV === "production" ? (
//   {
//     apiKey: "AIzaSyD-otsl4DBOpjeK066gy_Ftb8z3Ql5vsXo",
//     authDomain: "reward-joker-game.firebaseapp.com",
//     projectId: "reward-joker-game",
//     storageBucket: "reward-joker-game.appspot.com",
//     messagingSenderId: "973989339325",
//     appId: "1:973989339325:web:27c57e4411844a37f557d4",
//     measurementId: "G-HJH67SPKGG"
//   }
// ) : (
//   {
//     apiKey: "AIzaSyA5bYdRlzAQ-FbSsWmkSv7dOzFD6QZ9oRo",
//     authDomain: "dev-reward-joker-game.firebaseapp.com",
//     projectId: "dev-reward-joker-game",
//     storageBucket: "dev-reward-joker-game.appspot.com",
//     messagingSenderId: "879576131330",
//     appId: "1:879576131330:web:aa03061046c9fbf505441b"
//   }
// )

const firebaseConfig = {
  apiKey: "AIzaSyA5bYdRlzAQ-FbSsWmkSv7dOzFD6QZ9oRo",
  authDomain: "dev-reward-joker-game.firebaseapp.com",
  projectId: "dev-reward-joker-game",
  storageBucket: "dev-reward-joker-game.appspot.com",
  messagingSenderId: "879576131330",
  appId: "1:879576131330:web:aa03061046c9fbf505441b"
}


if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase
export const firestore = firebase.firestore()
export const database = firebase.database()