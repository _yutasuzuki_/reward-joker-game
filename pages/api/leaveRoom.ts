import type { NextApiRequest, NextApiResponse } from 'next'

const sleep = (num: number) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true)
    }, num)
  })
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.body) {
    res.status(200)
  } else {
    res.status(200).json({ text: 'Hello!' })
  }
}
