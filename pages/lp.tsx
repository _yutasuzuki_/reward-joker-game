import { GlobalLayout } from 'components/GlobalLayout/GlobalLayout'
import Link from 'next/link'
import { CARD_TEXT } from '../constants'
import styles from './lp.module.css'
import utilStyles from 'styles/utils.module.css'
import { useCallback, useEffect, useState } from 'react'
import { shuffle } from 'utils/ArrayUtil'
import { ShareURL } from 'components/ShareURL/ShareURL'

export default function Page() {
  const [cardText, setCardText] = useState<string>(null)
  const [copy, setCopy] = useState<boolean>(false)

  useEffect(() => {
    setCardText(shuffle(CARD_TEXT)[0])
  }, [])

  const _handleOnShare = useCallback(() => {
    if (!navigator.share) return
    navigator.share({
      title: 'Reward Joker Game',
      text: 'Reward Joker Game - 報酬ババ抜き',
      url: 'https://liff.line.me/1655594777-A0eDyaND',
    })
    .catch((error) => {
      // シェアせず終了した場合もここに入ってくる。
      console.log('Error sharing', error)
    })
  }, [])

  const _handleOnCopy = useCallback(() => {
    setCopy(true)
    const q = setTimeout(() => {
      clearTimeout(q)
      setCopy(false)
    }, 1200)
  }, [])

  return (
    <>
      <GlobalLayout>
        <section className={styles.mainImage}>
          <img src="/images/hands-empty.svg" />
          <p className={styles.cardText}>{cardText}</p>
        </section>
        <ShareURL
          onShare={_handleOnShare}
          onCopy={_handleOnCopy}
        />
        <section>
          <h1 className={utilStyles.subTitle}>
            <span className={styles.title}>Reward Joker Game</span>
            <span className={styles.japaneseTtile}>- 報酬ババ抜き -</span>
          </h1>
          <p className={styles.content}>
            Reward Joker Game - 報酬ババ抜き - は通常のババ抜きに加えてカードに報酬を設定し、揃える事で、その報酬を受け取る事ができるという義務が発生するゲームです。
          </p>
          <p className={styles.content}>
            あまり無茶な報酬は設定しないように気をつけましょう。
          </p>
          <div className={styles.artwork}>
            <img src="/images/jester.svg" />
          </div>
        </section>
        <section>
          <h2 className={utilStyles.subTitle}>ゲームのはじめ方</h2>
          <p className={styles.content}>
            始める前にババ抜きを行うメンバーで<span className={styles.strong}>LINEでグループを作成</span>してください。
          </p>
          <p className={styles.content}>
            URLを作成したグループにシェアしてください。シェアする方法は２つあります。
          </p>
          <div className={styles.content}>
            <ol className={styles.ol}>
              <li>「LINEにURLをシェアする」をクリックしLINEのグループに共有する。</li>
              <li>「コピー」をクリックしてグループへURLを投稿する。</li>
            </ol>
          </div>
          <div className={styles.artwork}>
            <img src="/images/chat.svg" />
          </div>
        </section>
        <section>
          <h2 className={utilStyles.subTitle}>ゲームのあそび方</h2>
          <div className={styles.content}>
            <ol className={styles.ol}>
              <li>LINEのグループ内に貼られたURLをクリックします。</li>
              <li>初回はログインを求められるので、ログインしてください。</li>
              <li>参加するボタンをクリックし、メンバーが揃ったら「このメンバーでゲームを開始する」をクリックしてください。</li>
              <li>カード毎に報酬を設定します。報酬は設定しなくても大丈夫です。</li>
              <li>設定したら送信します。全員が揃ったらゲームが開始されます。</li>
              <li>ゲームが終了すると結果画面で全員の獲得した報酬を確認できます。</li>
            </ol>
          </div>
          <div className={styles.artwork}>
            <img src="/images/cards.svg" />
          </div>
        </section>
        <ShareURL
          onShare={_handleOnShare}
          onCopy={_handleOnCopy}
        />
      </GlobalLayout>
      {copy && <div className={styles.copied}>URLをコピーしました</div>}
    </>
  )
}
