import Document, { Head, Html, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="ja">
        <Head>
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=M+PLUS+Rounded+1c:wght@500&Noto+Sans+JP:wght@300;400;700&family=Poppins:ital,wght@1,700&display=swap" rel="stylesheet" />
          <link rel="stylesheet" href="/css/reset.css" />
          <link rel="stylesheet" href="/css/default.css" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}