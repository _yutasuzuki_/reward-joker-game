import Head from 'next/head'
import Link from 'next/link'
import { Join } from 'containers/Join/Join'
import { MakeCards } from 'containers/MakeCards/MakeCards'
import { Play } from 'containers/Play/Play'
import { Result } from 'containers/Result/Result'
import { useAuth } from 'hooks/useAuth'
import { useScene } from 'hooks/useScene'
import { useMembers } from 'hooks/useMembers'
import { useGame } from 'hooks/useGame'
import { useResult } from 'hooks/useResult'
import { useTouchCard } from 'hooks/useTouchCard'
import ClipLoader from "react-spinners/ClipLoader"
import styles from './index.module.css'
import utilStyles from 'styles/utils.module.css'

export default function Page() {
  const { room, user } = useAuth()
  const { scene, sceneLoaded } = useScene(room, user)
  const { members } = useMembers(room, user)
  const { game, gameLoaded } = useGame(room, user)
  const { result, resultLoaded } = useResult(room, user)
  const { touchCard } = useTouchCard(room, user)

  if (!sceneLoaded || !gameLoaded || !resultLoaded || !user || !room) {
    return (
      <div className={styles.centerContainer}>
        <ClipLoader color={'#E5E5E5'} loading={true} size={64} />
      </div>
    )
  }

  if (!scene || scene.scene === 0) {
    return (
      <Join
        room={room}
        user={user}
        scene={scene}
        members={members}
      />
    )
  }

  if (!scene.members.find(id => id === user.userId)) {
    return (
      <div className={styles.centerContainer}>
        <div className={utilStyles.subText}>現在ゲーム中です。終わるまでお待ちください。</div>
      </div>
    )
  }

  if (scene.scene === 1) {
    return (
      <MakeCards
        room={room}
        user={user}
        scene={scene}
        members={members}
      />
    )
  }

  if (scene.scene === 3 || result.dones.find((id) => id === user.userId)) {
    return (
      <Result
        room={room}
        user={user}
        scene={scene}
        members={members}
        result={result}
      />
    )
  }

  if (scene.scene === 2) {
    return (
      <Play
        room={room}
        user={user}
        scene={scene}
        members={members}
        game={game}
        result={result}
        touchCard={touchCard}
      />
    )
  }


  return <div />
}
