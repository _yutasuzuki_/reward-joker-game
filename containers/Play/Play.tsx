import Head from 'next/head'
import Link from 'next/link'
import { GameLayout } from 'components/GameLayout/GameLayout'
import { database } from 'lib/firebase'
import styles from './Play.module.css'
import utilStyles from 'styles/utils.module.css'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { FaceupCard } from 'components/FaceupCard/FaceupCard'
import { FacedownCard } from 'components/FacedownCard/FacedownCard'
import { PairCard } from 'components/PairCard/PairCard'
import { Modal } from 'components/Modal/Modal'
import { Discards } from 'components/Discards/Discards'
import { removeDuplicateCards, getDuplicateCards, getDiscardPosition } from 'utils/CardUtil'
import { shuffle } from 'utils/ArrayUtil'

type Props = {
  room: string
  user: UserState
  scene: SceneState
  members: UserState[]
  game: GameState
  result: ResultState
  touchCard: TouchCardState
}

export const Play: React.FC<Props> = ({ room, user, members, game, result, touchCard }) => {
  const [firstModal, setFirstModal] = useState<boolean>(false)
  const [winModal, setWinModal] = useState<boolean>(false)
  const [loseModal, setloseModal] = useState<boolean>(false)
  const [clickable, setClickable] = useState<boolean>(true)
  const [drawCard, setDrawCard] = useState<CardState>(null)
  const [myHands, setMyHands] = useState<CardState[]>([])
  const [discards, setDiscards] = useState<CardState[]>([])

  useEffect(() => {
    const mount = async () => {
      if (room && user.userId && game.cards[user.userId]) {
        const cards = getDuplicateCards(game.cards[user.userId])
        if (cards.length) {
          const params = { ...game }
          params.cards[user.userId] = removeDuplicateCards(game.cards[user.userId])
          await database.ref(`games/${room}`).set(params)
          setDiscards(cards)
          setFirstModal(true)
        }
      }
    }
    mount()
    return () => {
      mount()
    }
  }, [game, user, room])

  useEffect(() => {
    const mount = async () => {
      if (user && game && result) {
        if (!game.cards[user.userId] && !drawCard) {
          setWinModal(true)
        }

        if (
          game.cards[user.userId] 
          && game.cards[user.userId].length === 1 
          && game.cards[user.userId].find(card => card.id === 'joker')
        ) {
          setloseModal(true)
        }
      }
    }
    mount()
    return () => {
      mount()
    }
  }, [user, game, result, drawCard])

  const isMyTurn = useMemo(() => {
    return user.userId === game.current_user
  }, [user, game])

  const nextUser = useMemo(() => {
    if (game && members.length && user.userId) {
      const index = game.order.indexOf(user.userId)
      let u = game.order[index + 1]
      if (!u) {
        u = game.order[0]
      }
      return u
    } else {
      return null
    }
  }, [members, game, user])

  const _handleOnTouchStart = useCallback((card: CardState) => () => {
    if (isMyTurn) {
      const params: TouchCardState = {
        nextUserId: nextUser,
        cardId: card.id
      }
      database.ref(`touchCard/${room}`).set(params)
    }
  }, [isMyTurn, nextUser])

  const _handleOnTouchEnd = useCallback(() => {
    database.ref(`touchCard/${room}`).remove()
  }, [user, game])

  const _handleOnDiscard = async () => {
    const resultParams = { ...result }
    resultParams.discards[user.userId] = discards.map((card) => {
      card.discardStyle = getDiscardPosition()
      return card
    })
    database.ref(`results/${room}`).set(resultParams)
    setFirstModal(false)
  }

  const _handleOnCloseWinModal = async () => {
    const resultParams = { ...result }
    if (resultParams.dones.length === members.length - 1) {
      resultParams.dones = [...resultParams.dones, user.userId]
    } else if (resultParams.dones.length === members.length - 2) {
      const loseUserId = game.order.filter((id) => id !== user.userId)
      resultParams.dones = [...resultParams.dones, user.userId, ...loseUserId]
    }
    database.ref(`results/${room}`).set(resultParams)
    database.ref(`scenes/${room}`).update({ scanes: 3 })
    setWinModal(false)
  }

  const _handleOnCloseLoseModal = async () => {
    const resultParams = { ...result }
    if (resultParams.dones.length === members.length - 1) {
      resultParams.dones = [...resultParams.dones, user.userId]
    } else if (resultParams.dones.length === members.length - 2) {
      const winUserId = game.order.filter((id) => id !== user.userId)
      resultParams.dones = [...resultParams.dones, ...winUserId, user.userId]
    }
    database.ref(`results/${room}`).set(resultParams)
    database.ref(`scenes/${room}`).update({ scanes: 3 })
    setloseModal(false)
  }

  const _handleOnCloseDrawModal = async () => {
    if (!drawCard) return
    const gameParams = { ...game }
    const cards = game.cards[user.userId]
    const drawCards = [...cards, drawCard]
    gameParams.current_user = nextUser
    gameParams.cards[nextUser] = gameParams.cards[nextUser].filter(c => c.id !== drawCard.id)
    gameParams.cards[user.userId] = removeDuplicateCards(drawCards)
    if (!gameParams.cards[user.userId].length) {
      gameParams.order = gameParams.order.filter(id => id !== user.userId)
    }

    const discards = getDuplicateCards(drawCards)
    await database.ref(`games/${room}`).set(gameParams)
    if (discards.length) {
      const resultParams = { ...result }
      const item = result.rewards[user.userId] || []
      discards[0].discardStyle = getDiscardPosition()
      item.push(discards[0])
      resultParams.rewards[user.userId] = item
      if (!gameParams.cards[user.userId].length) {
        resultParams.dones = [...resultParams.dones, user.userId]
      }
      await database.ref(`results/${room}`).set(resultParams)
    }

    setDiscards([])
    setClickable(true)
    setDrawCard(null)
  }

  const myCards = useMemo(() => {
    if (game.cards[user.userId]) {
      return game.cards[user.userId].map((card, index) => {
        let touchStyle = {}
        if (touchCard && touchCard.cardId === card.id && touchCard.nextUserId === user.userId) {
          touchStyle = { transform: `translateY(-12px)` }
        }
        return (
          <li key={index} className={styles.myCard} style={touchStyle}>
            <FaceupCard card={card} />
          </li>
        )
      })
    }
  }, [game, user, touchCard])

  const _handleOnSelectCard = useCallback((card: CardState) => async () => {
    if (isMyTurn && clickable) {
      setClickable(false)
      database.ref(`touchCard/${room}`).remove()
      const drawCards = [...game.cards[user.userId], card]
      setMyHands(removeDuplicateCards(drawCards))
      setDiscards(getDuplicateCards(drawCards))
      setDrawCard(card)
    }
  }, [user, game, clickable, isMyTurn])

  const otherCards = useMemo(() => {
    if (nextUser && game.cards[nextUser]) {
      return shuffle(game.cards[nextUser]).map((card, index) => {
        return (
          <li 
            key={index} 
            className={styles.otherCard} 
            onClick={_handleOnSelectCard(card)}
            onTouchStart={_handleOnTouchStart(card)}
            onTouchEnd={_handleOnTouchEnd}
            onMouseEnter={_handleOnTouchStart(card)}
            onMouseLeave={_handleOnTouchEnd}
          >
            <FacedownCard card={card} />
          </li>
        )
      })
    }
  }, [game, nextUser])

  const currentUserName = useMemo(() => {
    if (!game || !members.length) return ''
    const u = members.find((member) => member.userId === game.current_user)
    return u.displayName
  }, [members, game])

  const loser = useMemo(() => {
    return game.order.length === 1 && game.order[0] === user.userId
  }, [game, user])

  return (
    <>
      <GameLayout room={room}>
        <div className={styles.currentUser}>{currentUserName}さんの順番です</div>
        <div className={styles.playContainer}>
          <div className={styles.otherCardsContainer}>
            {!loser && (
              <ul className={styles.cardList}>
                {otherCards}
              </ul>
            )}
          </div>
          <Discards rewards={result.rewards} discards={result.discards} />
        </div>
        <div className={styles.myCardsContainer}>
          <ul className={styles.cardList}>
            {myCards}
          </ul>
        </div>
      </GameLayout>
      <Modal show={firstModal}>
        <div>
          <div className={utilStyles.modalTitle}>ペアになったカードを捨てます</div>
          <ul className={styles.cardList}>
            {discards.map((card, index) => {
              return (
                <li key={index} className={styles.myCard}>
                  <PairCard card={card} />
                </li>
              )
            })}
          </ul>
          <div className={utilStyles.btnContainer}>
            <a className={utilStyles.btn} onClick={_handleOnDiscard}>OK</a>
          </div>
        </div>
      </Modal>
      <Modal show={!!drawCard}>
        <div>
          <div className={utilStyles.modalTitle}>{discards.length ? 'ペアができました' : 'カードを引きました'}</div>
          <ul className={styles.cardList}>
            {discards.length ? discards.map((c, index) => <PairCard key={index} card={c} />) : <FaceupCard card={drawCard} />}
          </ul>
          {!myHands.length && (
            <div className={styles.winText}>あがりました</div>
          )}
          <div className={utilStyles.btnContainer}>
            <a className={utilStyles.btn} onClick={_handleOnCloseDrawModal}>OK</a>
          </div>
        </div>
      </Modal>
      <Modal show={winModal}>
        <div>
          <div className={styles.winText}>あがりました</div>
          <div className={utilStyles.btnContainer}>
            <a className={utilStyles.btn} onClick={_handleOnCloseWinModal}>OK</a>
          </div>
        </div>
      </Modal>
      <Modal show={loseModal}>
        <div>
          <div className={styles.loseText}>負けました</div>
          <div className={utilStyles.btnContainer}>
            <a className={utilStyles.btn} onClick={_handleOnCloseLoseModal}>OK</a>
          </div>
        </div>
      </Modal>
    </>
  )
}
