import { GameLayout } from 'components/GameLayout/GameLayout'
import styles from './Join.module.css'
import utilStyles from 'styles/utils.module.css'
import { useCallback, useEffect, useMemo } from 'react'
import { database } from 'lib/firebase'
import { UserJoin } from 'components/UserJoin/UserJoin'

type Props = {
  room: string
  user: UserState
  scene: SceneState
  members: UserState[]
}

export const Join: React.FC<Props> = ({ room, user, members }) => {

  const _handleOnJoinMember = useCallback(async () => {
    let params = [user]
    const u = members.find((member) => member.userId === user.userId)
    if (!u) {
      params = [...members, user]
    }
    database.ref(`members/${room}`).set(params)  
  }, [members])

  const _handleOnLeaveMember = useCallback(() => {
    if (!members) return
    const result = members.find((member) => member.userId === user.userId)
    if (room && result) {
      const items = members.filter((member) => member.userId !== user.userId)
      database.ref(`members/${room}`).set(items)
    }
  }, [members])

  const isJoin = useMemo(() => {
    const member = members?.find((member) => member.userId === user.userId)
    let bool = false
    if (member) {
      bool = true
    }
    return bool
  }, [members, user])

  const membersList = useMemo(() => {
    return members?.map((member, index) => {
      return (
        <UserJoin key={index} member={member} user={user} onLeave={_handleOnLeaveMember} />
      )
    })
  }, [members, user])

  const _handleOnSubmit = useCallback(() => {
    if (room && members?.length) {
      database.ref(`scenes/${room}`).set({
        scene: 1,
        members: members.map(member => member.userId)
      })
    }
  }, [room, members])

  const renderBtn = useMemo(() => {
    if (isJoin) {
      if (1 < members.length) {
        return (
          <div className={utilStyles.stickyBottomContainer}>
            <a className={utilStyles.btn} onClick={_handleOnSubmit}>このメンバーでゲームを開始する</a>
          </div>
        )
      }
      return (
        <div className={utilStyles.stickyBottomContainer}>
          <p className={utilStyles.subText}>他のメンバーが参加するまでお待ちください</p>
        </div>
      )
    }
    return null
  }, [isJoin, members])

  return (
    <GameLayout room={room}>
      <div>
        <div className={styles.mainImage}>
          <img src="/images/group.svg" />
        </div>
        {!isJoin && (
          <div className={utilStyles.btnContainer}>
            <a className={utilStyles.btn} onClick={_handleOnJoinMember}>ゲームに参加する</a>
          </div>
        )}
        <h2 className={utilStyles.subTitle}>参加メンバー</h2>
        {members.length ? (
          <ul className={`${utilStyles.list} ${utilStyles.marginBottom88}`}>
            {membersList}
          </ul>
        ) : (
          <div className={utilStyles.subText}>まだメンバーがいません</div>
        )}
        {renderBtn}
      </div>
    </GameLayout>
  )
}
