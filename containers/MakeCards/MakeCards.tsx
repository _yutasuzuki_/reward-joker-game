import { GameLayout } from 'components/GameLayout/GameLayout'
import { database } from 'lib/firebase'
import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { shuffle } from 'utils/ArrayUtil'
import styles from './MakeCards.module.css'
import utilStyles from 'styles/utils.module.css'
import { JOKER_DATA, REWARDS } from '../../constants'

type Props = {
  room: string
  user: UserState
  scene: SceneState
  members: UserState[]
}

export const MakeCards: React.FC<Props> = ({ room, user, members }) => {
  const [submited, setSubmited] = useState<boolean>(false)
  const [lastUser, setLastUser] = useState<boolean>(false)
  const [texts, setTexts] = useState<string[]>(['', '', '', '', ''])

  useEffect(() => {
    const onCardHandler = (snapshot) => {
      const data = snapshot.val() as CardState[] || []
      const cardMaxLength = members.length * 10
      if (data.length === (cardMaxLength - 10)) {
        setLastUser(true)
      } else {
        setLastUser(false)
      }
    }
    const mount = async () => { 
      if (room && user.userId) {
        database.ref(`cards/${room}`).on('value', onCardHandler)
        const snapshot = await database.ref(`cards/${room}`).get()
        if (snapshot) {
          const data = snapshot.val() as CardState[] || []
          const u = data.find(d => d.user.userId === user.userId)
          if (u) {
            setSubmited(true)
          }
        }
      }
    }
    mount()
    return () => {
      database.ref(`cards/${room}`).off('value', onCardHandler)
    }
  }, [room, user])

  const _handleOnChangeText = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => {
    const { value, name } = e.currentTarget
    setTexts((txts) => {
      const items = [...txts]
      items[Number(name)] = value
      return items
    })
  }, [])

  const _handleOnSubmit = async () => {
    const res = await database.ref(`cards/${room}`).get()
    const data = res.val() as CardState[] || []
    const items = texts.map((text, index) => {
      return {
        id: `${user.userId}_${index}`,
        number: index,
        colorCode: `#${user.userId.slice(-6)}`,
        text,
        user,
      }
    })
    const pairs = [...items, ...items]
    const shuffleCards = shuffle([...data, ...pairs])
    database.ref(`cards/${room}`).set(shuffleCards)
    setSubmited(true)
    if (lastUser) {
      const cards = {}
      const jokerIndex = Math.floor( Math.random() * members.length)
      members.forEach((member, index) => {
        let userCards = shuffleCards.slice(index * 10, index * 10 + 10)
        if (jokerIndex === index) {
          userCards = shuffle([...userCards, JOKER_DATA])
        }
        cards[member.userId] = userCards
      })
      const order = shuffle(members.map((member) => member.userId))
      const params = {
        cards,
        order,
        current_user: order[0]
      }
      database.ref(`games/${room}`).set(params)
      database.ref(`scenes/${room}`).update({ scene: 2 })
    }
  }

  const _handleOnClickAutoWriting = useCallback(() => {
    setTexts(shuffle(REWARDS).slice(0, 5))
  }, [])

  return (
    <GameLayout room={room}>
      <div className={`${utilStyles.relative} ${utilStyles.marginBottom32}`}>
        <h2 className={utilStyles.subTitle}>報酬カードの設定</h2>
        <a className={styles.btnAutoWriting} onClick={_handleOnClickAutoWriting}>自動入力</a>
      </div>
      {submited ? (
        <p className={utilStyles.subText}>全員の送信が終わるまでお待ちください</p>
      ): (
        <div className={`${utilStyles.container} ${utilStyles.marginBottom88}`}>
          <div className={styles.content}>
            <div className={styles.row}>
              <div className={utilStyles.relative}>
                <label className={styles.label} htmlFor="award-1">報酬 1</label>
                <div className={styles.count}>{texts[0].length}文字</div>
              </div>
              <textarea id="award-1" className={`${utilStyles.input} ${styles.input}`} onChange={_handleOnChangeText} name="0" value={texts[0]} />
            </div>
            <div className={styles.row}>
              <div className={utilStyles.relative}>
                <label className={styles.label} htmlFor="award-2">報酬 2</label>
                <div className={styles.count}>{texts[1].length}文字</div>
              </div>
              <textarea id="award-2" className={`${utilStyles.input} ${styles.input}`} onChange={_handleOnChangeText} name="1" value={texts[1]} />
            </div>
            <div className={styles.row}>
              <div className={utilStyles.relative}>
                <label className={styles.label} htmlFor="award-3">報酬 3</label>
                <div className={styles.count}>{texts[2].length}文字</div>
              </div>
              <textarea id="award-3" className={`${utilStyles.input} ${styles.input}`} onChange={_handleOnChangeText} name="2" value={texts[2]} />
            </div>
            <div className={styles.row}>
              <div className={utilStyles.relative}>
                <label className={styles.label} htmlFor="award-4">報酬 4</label>
                <div className={styles.count}>{texts[3].length}文字</div>
              </div>
              <textarea id="award-4" className={`${utilStyles.input} ${styles.input}`} onChange={_handleOnChangeText} name="3" value={texts[3]} />
            </div>
            <div className={styles.row}>
              <div className={utilStyles.relative}>
                <label className={styles.label} htmlFor="award-5">報酬 5</label>
                <div className={styles.count}>{texts[4].length}文字</div>
              </div>
              <textarea id="award-5" className={`${utilStyles.input} ${styles.input}`} onChange={_handleOnChangeText} name="4" value={texts[4]} />
            </div>
          </div>
          <div className={utilStyles.stickyBottomContainer}>
            <a className={utilStyles.btn} onClick={_handleOnSubmit}>{lastUser ? 'ゲームを開始する' : '送信'}</a>
          </div>
        </div>
      )}
    </GameLayout>
  )
}
