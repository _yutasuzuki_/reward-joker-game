import Head from 'next/head'
import Link from 'next/link'
import { GameLayout } from 'components/GameLayout/GameLayout'
import { useCallback, useMemo } from 'react'
import { UserRank } from 'components/UserRank/UserRank'
import utilStyles from 'styles/utils.module.css'
import styles from './Result.module.css'
import { database } from 'lib/firebase' 

type Props = {
  room: string
  user: UserState
  scene: SceneState
  members: UserState[]
  result: ResultState
}

export const Result: React.FC<Props> = ({ room, user, result, members, scene  }) => {

  const memberRanking = useMemo(() => {
    if (members.length) {
      return result.dones.map((id) => {
        return members.find(member => member.userId === id)
      }).map((member, index) => {
        const reward = result.rewards[member.userId] || []
        return (
          <UserRank key={index} user={member} index={index} reward={reward} />
        )
      })
    }
    return []
  }, [members, result])

  const _handleOnReset = useCallback(() => {
    if (window.confirm("TOPに戻ってもよろしいですか？")) {
      database.ref(`games/${room}`).remove()
      database.ref(`members/${room}`).remove()
      database.ref(`cards/${room}`).remove()
      database.ref(`results/${room}`).remove()
      database.ref(`scenes/${room}`).set({ scene: 0, members: [] })
    }
  }, [room])

  return (
    <>
      <GameLayout room={room}>
        <div className={utilStyles.subTitle}>ゲーム結果</div>
        <ul className={utilStyles.list}>
          {memberRanking}
        </ul>
        <div className={utilStyles.container}>
          <a onClick={_handleOnReset}>TOPに戻る</a>
        </div>
      </GameLayout>
    </>
  )
}