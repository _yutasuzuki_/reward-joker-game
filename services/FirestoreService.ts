import { firestore } from 'lib/firebase'

class FirestoreServiceClass {
  constructor() {}

  async getGame(id: string) {
    const ref = firestore.collection("games").doc(id)
    const snapshot = await ref.get()
    const game = snapshot.data() as Partial<GameState>
    return game
  }
}

export const FirestoreService = new FirestoreServiceClass