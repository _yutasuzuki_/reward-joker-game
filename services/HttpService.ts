
class HttpServiceClass {
  constructor() {
    
  }

  post<T>(url, data: any): Promise<{ result: T }> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await fetch(url, {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json'
          },
          redirect: 'follow',
          referrerPolicy: 'no-referrer',
          body: JSON.stringify(data)
        })
        resolve(response.json())
      } catch(error) {
        reject(error)
      }
    })
  }

  get<T>(url): Promise<{ result: T }> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await fetch(url)
        resolve(response.json())
      } catch(error) {
        reject(error)
      }
    })
  }
}

export const HttpService = new HttpServiceClass