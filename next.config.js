const isProduction = process.env.NODE_ENV === "production"

module.exports = {
  env: {
    liffId: isProduction ? "1655594777-A0eDyaND" : "1655594971-ZjbdrAnY",
  },
}