type UserState = {
  pictureUrl: string
  userId: string
  displayName: string
}

type CardState = {
  id: string
  number: number
  colorCode: string
  text: string
  user: UserState
  discardStyle?: {
    top: string
    left: string
    frontRotate: string
    backRotate: string
  }
}

type GameState = {
	current_user: UserState['userId'],
	order: UserState['userId'][],
  cards: {
    [key: string]: CardState[]
  }
}

type MembersState = {
  room: string
  members: UserState[]
}

type SceneState = {
  scene: number
  members: UserState['userId'][]
}

type ResultState = {
  dones: UserState['userId'][]
  discards: {
    [key: string]: CardState[]
  }
  rewards: {
    [key: string]: CardState[]
  }
}

type TouchCardState = {
  nextUserId: UserState['userId'] 
  cardId: CardState['id']
}